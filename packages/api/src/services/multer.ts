import multer from "multer";
import path from "path";
import { fileDirectory } from "../server";
import { v4 as uuidv4 } from "uuid";
const storage = multer.diskStorage({
  destination: (req: any, file: any, cb: any) => {
    cb(null, fileDirectory);
  },
  filename: (req: any, file: any, cb: any) => {
    cb(null, uuidv4() + path.extname(file.originalname));
  },
});
const imageUpload = multer({
  storage,
  fileFilter: (req: any, file: any, callback: any) => {
    const ext = path.extname(file.originalname);
    if (ext !== ".png" && ext !== ".jpg" && ext !== ".gif" && ext !== ".jpeg") {
      return callback("unsupported image", null);
    }
    callback(null, true);
  },
});
export { storage, imageUpload };
