import jwt from "jsonwebtoken";
import { Role } from "./domain/user/UserModel";
const JWT_SECRET = "jWt$3cr3t";
const JWT_EXPIRES_IN = "1d";

export const decodeJWT = (token: string = ""): Promise<{ email: string }> => {
  if (token.startsWith("Bearer ")) {
    token = token.slice(7, token.length);
  }
  return new Promise((resolve, reject) =>
    jwt.verify(token, JWT_SECRET, (err: any, decoded: any) => {
      if (decoded) {
        resolve(decoded);
      }
      if (err) {
        throw reject(err);
      }
      return reject(null);
    })
  );
};

export const encodeJWT = ({
  email,
  role,
  id,
}: {
  email: string;
  role: Role;
  id: string;
}) =>
  jwt.sign(
    {
      email,
      role,
      id,
    },
    JWT_SECRET,
    {
      expiresIn: JWT_EXPIRES_IN,
    }
  );
