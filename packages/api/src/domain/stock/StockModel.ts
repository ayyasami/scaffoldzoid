import { InstanceType, ModelType, prop, Ref, Typegoose } from "typegoose";
import { Orange } from "../orange/OrangeModel";
import { User } from "../user/UserModel";

export class Stock extends Typegoose {
  @prop()
  public seller: Ref<User>;
  @prop()
  public orange: Ref<Orange>;
  @prop()
  public price: number;
  @prop({ default: true })
  public availablity: boolean;
}

export const StockModel = new Stock().getModelForClass(Stock, {
  schemaOptions: {
    shardKey: { _id: 1 },
    collection: "stock",
    timestamps: { createdAt: true, updatedAt: true },
  },
});
