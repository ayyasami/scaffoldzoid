import { Router } from "express";
import { addStock, getStock, updateStock } from "./StockController";
export const stockRouter = Router();

stockRouter.put("/update", updateStock);
stockRouter.post("/add", addStock);
stockRouter.get("/", getStock);
