import { Response } from "express";
import { AuthRequest } from "../user/UserController";
import { StockModel } from "./StockModel";

export const getStock = (request: AuthRequest, response: Response) =>
  StockModel.find()
    .then((data: any) => {
      response.status(200).send({
        status: "success",
        data,
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );

export const addStock = (request: AuthRequest, response: Response) => {
  const { orange, price, availablity } = request.body;
  return new StockModel({
    orange,
    price,
    availablity,
    seller: request.user?.id,
  })
    .save()
    .then((data: any) => {
      response.status(200).send({
        status: "success",
        message: "succesfully created user",
        data,
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );
};
export const updateStock = (request: AuthRequest, response: Response) => {
  const { id, orange, price, availablity } = request.body;

  return StockModel.findOneAndUpdate(
    { _id: id },
    { orange, price, availablity }
  )
    .then((data: any) => {
      response.status(200).send({
        status: "success",
        data,
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );
};
