import { Router } from "express";
import { addOrange, getOrange } from "./OrangeController";
export const orangeRouter = Router();

orangeRouter.post("/add", addOrange);
orangeRouter.get("/", getOrange);
