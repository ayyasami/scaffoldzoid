import { prop, Ref, Typegoose } from "typegoose";
import { User } from "../user/UserModel";

export class Orange extends Typegoose {
  @prop()
  public name: string;
  @prop()
  public description?: string;
  @prop()
  public origin?: string;
  @prop()
  public createdBy: Ref<User>;
}

export const OrangeModel = new Orange().getModelForClass(Orange, {
  schemaOptions: {
    shardKey: { _id: 1 },
    collection: "orange",
    timestamps: { createdAt: true, updatedAt: true },
  },
});
