import { Response } from "express";
import { AuthRequest } from "../user/UserController";
import { OrangeModel } from "./OrangeModel";

const saveOrange = (orangeInfo: any) => {
  return new Promise((resolve, reject) => {
    OrangeModel.findOne({ name: orangeInfo.name }).then((orange: any) => {
      if (orange) {
        resolve(orange);
      } else {
        new OrangeModel({ ...orangeInfo }).save().then(resolve).catch(reject);
      }
    });
  });
};
export const getOrange = (request: AuthRequest, response: Response) =>
  OrangeModel.find()
    .then((data: any) => {
      response.status(200).send({
        status: "success",
        data,
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );

export const addOrange = (request: AuthRequest, response: Response) => {
  const { name, description, origin } = request.body;

  saveOrange({
    name,
    description,
    origin,
    createdBy: request.user && request.user.id,
  })
    .then((suc: any) => {
      response.status(200).send({
        status: "success",
        message: "succesfully created user",
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );
};
