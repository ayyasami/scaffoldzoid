import { Role, UserModel } from "./UserModel";
import { Request, Response, NextFunction } from "express";
import { decodeJWT, encodeJWT } from "../../utils";
export interface AuthRequest extends Request {
  user?: {
    email: string;
    role: Role;
    id: string;
  };
}

const saveUser = (userData: any) => {
  return new Promise((resolve, reject) => {
    UserModel.findOne({ email: userData.email }).then((user: any) => {
      if (user) {
        resolve(user);
      } else {
        new UserModel(userData).save().then(resolve).catch(reject);
      }
    });
  });
};

export const authCheck = (
  request: AuthRequest,
  response: Response,
  next: NextFunction
) => {
  const jwt = request.headers["x-access-token"];
  decodeJWT(jwt as string)
    .then((user: any) => {
      if (user) {
        request.user = user;
        next();
      } else {
        response.status(401).json({
          message: "unAuthenticated user",
        });
      }
    })
    .catch(() => {
      response.status(400).json({
        message: "unAuthenticated/bad requestuest",
      });
    });
};

export const handleUserSignup = (request: Request, response: Response) => {
  const { name, email, password, role } = request.body;

  saveUser({ name, email, password, role })
    .then((suc: any) => {
      response.status(200).send({
        status: "success",
        message: "succesfully created user",
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );
};

export const userProfileUpload = (request: AuthRequest, response: Response) => {
  const { path, size, filename }: any = request.file;
  UserModel.findOneAndUpdate(
    { email: request.user && request.user.email },
    { profileImage: filename }
  ).then((data: any) => {
    response.status(200).send({
      status: "success",
      message: "succesfully created user",
      data,
    });
  });
};

export const handleUserLogin = (request: Request, response: Response) => {
  const { email, password } = request.body;

  UserModel.findOne({ email, password })
    .then(({ role, _id: id }: any) => {
      const jwt = encodeJWT({ email, role, id });
      response.setHeader("x-access-token", `Bearer ${jwt}`);

      response.status(200).send({
        status: "success",
        token: jwt,
        message: "succesfully logged",
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );
};

export const handleUserData = async (request: Request, response: Response) => {
  const user = await decodeJWT(request.headers["x-access-token"] as string);
  UserModel.findOne({ email: user.email })
    .then((data: any) => {
      response.status(200).send({
        status: "success",
        data,
      });
    })
    .catch((error: any) =>
      response.status(500).send({
        status: "failure",
        error,
      })
    );
};
