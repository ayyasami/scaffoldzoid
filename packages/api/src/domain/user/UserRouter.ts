import { Router } from "express";
import { imageUpload } from "../../services/multer";
import {
  authCheck,
  handleUserData,
  handleUserLogin,
  handleUserSignup,
  userProfileUpload,
} from "./UserController";
export const userRouter = Router();

userRouter.post("/signup", handleUserSignup);
userRouter.post("/login", handleUserLogin);
userRouter.get("/", authCheck, handleUserData);
userRouter.use(
  "/uploads",
  authCheck,
  imageUpload.single("img"),
  userProfileUpload
);
