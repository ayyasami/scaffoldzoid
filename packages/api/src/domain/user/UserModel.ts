import { v4 as uuidv4 } from "uuid";
import { prop, Typegoose } from "typegoose";
export enum Role {
  seller = "SELLER",
  buyer = "BUYER",
}

export class User extends Typegoose {
  @prop()
  public name: string;
  @prop()
  public email: string;
  @prop()
  public password: string;
  @prop({ default: Role.buyer })
  public role: Role;
  @prop()
  public description: string;
  @prop()
  public profileImage: string;
}

export const UserModel = new User().getModelForClass(User, {
  schemaOptions: {
    shardKey: { _id: 1 },
    collection: "user",
    timestamps: { createdAt: true, updatedAt: true },
  },
});
