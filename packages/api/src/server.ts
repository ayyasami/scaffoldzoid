import bodyParser from "body-parser";
import { config } from "dotenv";
import express, { Express } from "express";
import fs from "fs";
import mongoose from "mongoose";
import path from "path";
import { orangeRouter } from "./domain/orange/OrangeRouter";
import { authCheck } from "./domain/user/UserController";
import { userRouter } from "./domain/user/UserRouter";
import { stockRouter } from "./domain/stock/StockRouter";
import cors from "cors";
[
  process.env.NODE_ENV === "development"
    ? `../.env.development.local`
    : `../.env.production.local`,
  "../.env.local",
  "../.env",
].forEach(
  (name) =>
    fs.existsSync(path.resolve(__dirname, name)) &&
    config({ path: path.resolve(__dirname, name) })
);
export const ASSETS_IMG_PATH = "/assets/img/";
export const fileDirectory = path.join(
  __dirname,
  process.env.STATIC_ASSETS_PATH as string
);

mongoose
  .connect(process.env.MONGO_URI || "", {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(
    (success: any) => console.log("Successfully Connected with MongoDB"),
    (error: any) => console.log("Connection failed MongoDB..!!!!!!")
  );
if (!fs.existsSync(fileDirectory)) {
  fs.mkdirSync(fileDirectory);
}

const app: Express = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use((req, res, next) => {
  res.header("X-powered-by", "Blood, sweat, and tears.");
  const allowedOrigins = ["http://127.0.0.1:3000", "http://localhost:3000"];
  const origin: string = req.headers.origin as string;
  if (allowedOrigins.includes(origin)) {
    res.setHeader("Access-Control-Allow-Origin", origin);
  }
  res.header("Access-Control-Allow-Methods", "GET, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

app.use(ASSETS_IMG_PATH, express.static(fileDirectory));
app.use("/user", userRouter);
app.use("/orange", authCheck, orangeRouter);
app.use("/stock", authCheck, stockRouter);

app.listen({ port: process.env.PORT }, () => {
  console.log(`🚀  Server ready at ${process.env.PORT}`);
});
