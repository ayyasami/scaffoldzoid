import React from "react";
import { Link, Redirect, Route, Switch, withRouter } from "react-router-dom";
import styled from "styled-components";
import { TokenContext } from '../App';
import { fetchOrange } from "../Http";
import { Login } from './Login';
import { Signup } from './Signup';
import { UserRoute } from './User';
const AppContainer = styled("div")`
  margin: auto;
  width: 80%;
  max-width: 900px;
  color: ##1267fb;
`;
const Header = styled("div")`
	justify-content: space-between;
	display: flex;
	align-items: center;
	a {
		text-decoration: none;
	font-size: 2rem;
	}
	span{
		margin: 0.2rem;
		border-radius: 5px;
		padding: .4rem;
		display: inline-block;
		vertical-align: middle;
		box-shadow: 0 0 1px transparent;
		position: relative;
		color: #1267fb;
		border: 1px solid #1267fb;
	}
}`;
const Introduction = styled("div")`
	margin: 11rem 0;
	padding: 1rem;
	font-size: xx-large;
	border: solid #7699f3 1px;
	box-shadow: -8px 9px 0px #c4d9fb;
`;
const AppShell = () => {
	return (
		<AppContainer>
			<Switch>
				<div>
					<NavBar />
					<Route exact={true} path="/" component={Home} />
					<Route path="/signup" component={Signup} />
					<Route path="/login" component={Login} />
					<PrivateRoute path="/user" component={UserRoute} />
					<PrivateRoute path="/protected" component={Protected} />
				</div>
			</Switch>
		</AppContainer>
	);
};
const Home = () => <Introduction>
	Scaffoldzoid is a company that is a bridges between orange sellers and orange buyers. As an orange seller, I can register in Scaffoldzoid and be listed as an orange
	seller
</Introduction>;

const StatusBar = styled("div")`
  justify-content: space-between;
  display: flex;
`;
const NavBar = withRouter(({ history }) => {
	const { token, clearToken } = React.useContext(TokenContext);
	return (
		<div>
			<Header>
				<Link to="/">Scaffoldzoid</Link>
				<div>
					{!token ?
						<div>
							<span onClick={() => history.push("/login?role=seller")}>Sell</span>
							<span onClick={() => history.push("/login?role=buyer")}>Buy</span>
						</div>
						: <div
							onClick={() => {
								clearToken();
							}}
						>
							<div>Sign out</div>
						</div>
					}				</div>
			</Header>
			<StatusBar />
		</div>
	);
});

const PrivateRoute = ({ component: Component, ...rest }: any) => {
	const { token } = React.useContext(TokenContext);

	return (
		<Route
			{...rest}
			render={(props) =>
				token ? (
					<Component {...props} />
				) : (
						<Redirect
							to={{
								pathname: "/login",
								state: { from: props.location },
							}}
						/>
					)
			}
		/>
	);
};

function OrangeRoute() {
	React.useEffect(() => {
		fetchOrange().then((resp: any) => console.log("__resp_", resp));
	}, []);
	return <div>Public</div>;
}

function Protected() {
	return <div>Protected</div>;
}



export default AppShell;
