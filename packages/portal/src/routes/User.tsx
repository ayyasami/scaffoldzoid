import * as React from 'react';
import { withRouter } from "react-router-dom";
import { fetchUserInfo } from '../Http';

export const UserRoute = withRouter((props) => {
	const [userBasicInfo, setUserBasicInfo] = React.useState<any>();
	React.useEffect(() => {
		fetchUserInfo().then(({data}: any) => setUserBasicInfo(data));
	}, []);
	if (!userBasicInfo) {
		return <div>fetching...</div>;
	}
	return (
		<div>
			Welcome {userBasicInfo.name}..!
		</div>
	);
});