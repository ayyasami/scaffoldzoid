import { getQuery, userSignup } from 'Http';
import * as React from 'react';
import { withRouter } from "react-router-dom";
import { ScaffyForm } from '../form/Form';

export const Signup = withRouter((props) => {
	return (
		<div>
			<ScaffyForm
				fields={[
					{ name: "name", label: "name", type: "text" },
					{ name: "email", label: "email", type: "text" },
					{ name: "password", label: "password", type: "password" },
					{ name: "password2", label: "confirm password", type: "password", validation: { rule: "isMatch", err: "password doesnt match"} },
				]}
				onSubmit={({ email, password, name }: any) => {
					const { role } = getQuery(props.location.search);
					console.log("role", role,props.location.search, getQuery(props.location.search));
					userSignup({ email, password, name, role: role as string}).then(()=>{
						props.history.push("/login");
					});
				}}
				onCancel={() => props.history.push("/")}
				submitLabel="Signup"
			/>
		</div>
	);
});