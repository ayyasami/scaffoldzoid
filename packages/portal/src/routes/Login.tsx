import { userLogin } from 'Http';
import * as React from 'react';
import { withRouter } from "react-router-dom";
import { TokenContext } from '../App';
import { ScaffyForm } from '../form/Form';

export const Login = withRouter((props) => {
	const { setToken } = React.useContext(TokenContext);
	return (
		<div>
			<ScaffyForm
				fields={[{ name: "email", label: "email/userName", type: "text", validation: { rule: "required", err: "required field" } },
				{ name: "password", label: "password", type: "password", validation: { rule: "required", err: "required field" } },]}
				onSubmit={({ email, password }: any) => {
					userLogin({ email, password }).then((data: any) => {
						props.history.push("/user");
						setToken({ token: data.token || null });
					});
				}}
				onCancel={() => props.history.push("/signup" + props.location.search)}
				cancelLabel={"Signup"}
				submitLabel="Login"
			/>
		</div>
	);
});