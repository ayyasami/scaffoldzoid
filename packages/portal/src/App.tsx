import React from "react";
import { BrowserRouter } from "react-router-dom";
import AuthExample from "routes/Routes";
import "./App.css";

interface TokenContextProps {
  token: string | null;
  clearToken: () => void;
  setToken: ({ token }: { token: string | null }) => void;
}
export const TokenContext = React.createContext<TokenContextProps>({
  token: localStorage.getItem("token") || null,
  clearToken: () => localStorage.clear(),
  setToken: ({ token }: any) => {
    localStorage.setItem("token", token);
  }
});

const App = () => {
  const [token, setToken] = React.useState(localStorage.getItem("token") || null);
  React.useEffect(() => {
    const tokenChecker = setInterval(() => {
      if (token !== localStorage.getItem("token")) {
        setToken(localStorage.getItem("token") || null);
      }
    }, 1000);
    return () => clearTimeout(tokenChecker);
  }, [token]);
  return (
    <BrowserRouter>
      <TokenContext.Provider value={{ token, clearToken: () => localStorage.clear(), setToken: ({ token }: any) => localStorage.setItem("token", token) }}>
        <AuthExample />
      </TokenContext.Provider>
    </BrowserRouter>
  );
};

export default App;
