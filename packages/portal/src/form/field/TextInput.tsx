import { Field, FieldProps as FormikFieldProps } from 'formik';
import * as React from 'react';
import styled from 'styled-components';
import { FieldProps, Label } from './Field';

interface TextInputProps extends FieldProps {}
interface StyledInputFieldProps extends TextInputProps {
  error: boolean;
}
export const StyledInputField = styled('input')<StyledInputFieldProps>`
  border-radius: 3px;
  padding: 0.5rem;
  background: #ffffff;
  border: 1px solid #ddd;

  outline: #ddd;
  -webkit-appearance: none;
  margin: 0.1rem 0;
  box-sizing: border-box;
  width: 100%;
  input[type='number']::-webkit-inner-spin-button,
  input[type='number']::-webkit-outer-spin-button,
  input[type='number']:hover::-webkit-outer-spin-button,
  input[type='number']:hover::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

export const TextInput: React.FC<TextInputProps> = (props) => (
  <Field name={props.name}>
    {(formikProps: FormikFieldProps) => {
      const {
        field,
        form: {
          touched: { [props.name]: touched },
          errors: { [props.name]: error },
        },
      } = formikProps;
      return (
        <div>
          {props.label && <Label {...props} />}
          <StyledInputField
            {...field}
            error={!!(touched && error)}
            type={props.type}
            placeholder={props.placeholder}
            disabled={props.disabled}
          />
        </div>
      );
    }}
  </Field>
);
