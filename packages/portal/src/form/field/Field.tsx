import { ErrorMessage } from 'formik';
import styled from 'styled-components';

export interface ValidationProps {
  rule: string;
  err: string;
}

export interface FieldProps {
  label?: string;
  type: 'text'|'password'|'number';
  name: string;
  placeholder?: string;
  value?: any;
  validation?: ValidationProps | (ValidationProps | undefined)[];
  disabled?: boolean;
}
const StyledLabel = styled('label')`
  font-size: 1rem;
  display: flex;
  justify-content: space-between;
  color: #2d2d2d;
  /*margin-top: 0.825rem;*/
  margin-bottom: 0.5rem;
  font-weight: 100;
  text-transform: capitalize;
`;
const RightLabel = styled('div')`
  display: flex;
  * {
    margin-left: 1rem;
  }
`;
export const StyledInputContainer = styled('div')`
  position: relative;
  align-items: center;
  display: flex;
  width: 100%;
  div {
    width: 100%;
  }
`;
const HeaderDiv = styled('span')<{ fieldtype: string }>`
  display: flex;
  cursor: pointer;
  justify-content: space-between;
  padding-top: 10px
  align-items: center;
  & > span {
    font-size: 1rem;
  }
}`;

export const StyledErrorMessage = styled(ErrorMessage)`
  margin-top: 0;
  font-size: 0.6rem;
  color: red;
  font-weight: normal;
`;

export const Label: React.FC<FieldProps> = (props) => (
  <StyledLabel>
    {props.label}
    <RightLabel>
      {props.children}
      <StyledErrorMessage name={props.name} component={'span'} />
    </RightLabel>
  </StyledLabel>
);
