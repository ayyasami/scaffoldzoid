import { Form, Formik } from 'formik';
import _noop from 'lodash/noop';
import * as React from 'react';
import styled from 'styled-components';
import { validator } from '../form/Rules';
import { FieldProps } from './field/Field';
import { TextInput } from './field/TextInput';
interface ScaffyFormProps {
  fields: FieldProps[];
  onSubmit?: (data: any) => void;
  submitLabel?: string;
  onCancel?: () => void;
  cancelLabel?: string;
}

const FieldDiv = styled('div')`
  margin: 0px;
`;
const FormAttrDiv = styled('div')``;

const renderFields = (fields: FieldProps[]) => {
  return fields.map((input: FieldProps) => {
    switch (input.type) {
      case 'text':
      case 'number':
      case 'password':
        return (
          <TextInput
            key={input.name}
            placeholder={input.placeholder || ''}
            {...input}
          />
        );

      default:
        return (
          <TextInput
            key={input.name}
            placeholder={input.placeholder || ''}
            {...input}
          />
        );
    }
  });
};
const getInitialValues = (fields: FieldProps[]) => {
  const initialValues: { [name: string]: any } = {};
  fields.forEach((field) => {
    if (!initialValues[field.name]) {
      initialValues[field.name] = field.value || '';
    }
  });
  return initialValues;
};
const getValidationRules = (fields: FieldProps[]) => {
  const validationRules: { [name: string]: any } = {};
  fields.forEach((field) => {
    if (!validationRules[field.name]) {
      validationRules[field.name] = !!field.validation
        ? Array.isArray(field.validation)
          ? field.validation
          : [field.validation]
        : undefined;
    }
  });
  return validationRules;
};
export const ScaffyForm: React.FC<ScaffyFormProps> = ({
  fields,
  onSubmit,
  submitLabel = 'Submit',
  cancelLabel = 'Cancel',
  onCancel,
}) => {
  const initialValues = getInitialValues(fields);
  const rules = getValidationRules(fields);
  const onSubmitHandler = !!onSubmit ? onSubmit : (data: any) => _noop;

  return (
    <Formik
      initialValues={initialValues}
      validate={(values) => validator(rules, values)}
      onSubmit={onSubmitHandler}
    // onSubmit={(values, actions) => {
    // 	// console.log({ values, actions });
    // 	onSubmit(values | {});
    // 	// alert(JSON.stringify(values, null, 2));
    // 	// actions.setSubmitting(false);
    // }}
    >
      <Form>
        <FormAttrDiv>
          {fields.length &&
            renderFields(fields).map((field: any, idx: number) => (
              <FieldDiv key={idx}>{field}</FieldDiv>
            ))}
        </FormAttrDiv>
        <button color="primary" type="submit">
          {submitLabel}
        </button>{' '}
        {cancelLabel && (
          <button color="secondary" onClick={onCancel}>
            {cancelLabel}
          </button>
        )}
      </Form>
    </Formik>
  );
};
