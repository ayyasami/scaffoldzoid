import { ValidationProps } from './field/Field';

export interface ValidationRule {
  value: string;
  err: string;
  [name: string]: any;
}

export const RULES: {
  [name: string]: (props: ValidationRule) => string | undefined;
} = {
  required: (props) => {
    if (!!!props.value) {
      return props.err;
    }
    return undefined;
  },
  isMatch: (props) => {
    if (props.value !== props.values.password) {
      return props.err;
    }
    return undefined;
  },
  minLength: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;
    if (value && value.length < props.minLength) {
      return props.err;
    }
    return undefined;
  },
  maxLength: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;
    if (value && value.length > props.maxLength) {
      return props.err;
    }
    return undefined;
  },
  isNumeric: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;
    if (value && !value.match(/[0-9]/)) {
      return props.err;
    }
    return undefined;
  },
  isAlpha: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;

    if (value && !value.match(/[a-zA-Z]/)) {
      return props.err;
    }
    return undefined;
  },
  isAlphaNumeric: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;
    if (value && !value.match(/[a-zA-Z0-9]/)) {
      return props.err;
    }
    return undefined;
  },
  isEmail: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;
    if (
      value &&
      !value.match(
        /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
      )
    ) {
      return props.err;
    }
    return undefined;
  },
  isMobileNumber: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;
    if (value && !value.match(/[6-9][0-9]{9}/)) {
      return props.err;
    }
    return undefined;
  },
  isRegex: (props) => {
    const value =
      typeof props.value === 'number'
        ? Number(props.value).toString()
        : props.value;
    if (value && !value.match(props.regex)) {
      return props.err;
    }
    return undefined;
  },
};

export const validator = (
  rules: { [name: string]: ValidationProps },
  values: { [name: string]: any }
) => {
  const err: { [name: string]: string | undefined } = {};
  Object.keys(rules).forEach((name: any) => {
    const value = values[name];
    const fieldRules: any = rules[name];
    if (!!fieldRules) {
      const fieldError = fieldRules.map((fRule: any) =>
        RULES[fRule.rule]({ values, value, ...fRule })
      );
      err[name] = fieldError.filter((e: any) => !!e)[0];
    } else {
      err[name] = undefined;
    }
  });
  Object.keys(err).forEach((key) => err[key] === undefined && delete err[key]);
  return err;
};
