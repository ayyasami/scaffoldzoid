import Axios from "axios";
import localForge from "localforage";
import qs from "querystring";
export const userInfo = localForge.createInstance({
	name: "userInfo"
});
const axios = (str = localStorage) => Axios.create({
	baseURL: "http://localhost:4000/",
	headers: {
		"x-access-token": str.getItem("token")
	}
});
export const getQuery = (str: string) => {
	return qs.parse(str.split('?')[1]);
};

export const userSignup = ({ email, password, name, role }: { name: string, role: string, email: string, password: string }) =>
	axios().post("user/signup", { email, password, name, role }).then(({ data }: any) => {
		window.afterEach(data.status);
		return data;
	});

export const userLogin = ({ email, password }: { email: string, password: string }) =>
	axios().post("user/login", { email, password }).then(({ data }: any) => {
		return data;
	});
export const fetchUserInfo = () =>
	axios().get("user").then(({ data }: any) => {
		return data;
	});


export const fetchOrange = async () => await axios().get("orange");
